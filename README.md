# QuantumDaily
## _System to manage Quantumbit projects and activities_

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

QuantumDaily is a web system developed with Laravel and VueJs.

## Features
- Login for users
- Create, edit and delete users
- Customer Management
- Management of user roles and permissions
- Management of projects and their activities
- Management of invoices for clients
- Send notifications and documents via email
- Management of user assistance

QuantumDaily mainly allows users to manage and work on various projects and activities by providing different functionalities that allow both administrators and employees to perform their respective tasks.

## Tech
QuantumDaily uses a number of projects to work properly:
- [VueJS] - A progressive JS framework for building user interfaces
- [VsCode] - code text editor
- [BootstrapVue] - great UI for VueJS web apps
- [Vuexy] - VueJs template based on BootstrapVue
- [Laravel] - php framework for backend
- [MySQL] - database manager
- [Node.js] - to run front server


## Installation

QuantumDaily requires [Node.js](https://nodejs.org/) v10+ to run. And check the official laravel installation guide for server requirements before you start (https://laravel.com/docs/8.x/installation). 

Install the dependencies and devDependencies and start the front server.

```sh
cd quantumdaily-front
npm i
npm run serve
```

For production environments...

```sh
npm install 
npm run build

```

Install the dependencies and devDependencies and start the back server (Via Composer).
```sh
cd quantumdaily-back
composer install
cp .env.example .env
php artisan key:generate
```
Make sure you set the correct database connection information before running the migrations Environment variables. .env - Environment variables can be set in this file.
And make sure to install laravel passport before run the server.

```sh
php artisan migrate
php artisan passport:install
php artisan serve
```
## Plugins

Dillinger is currently extended with the following plugins and libraries on the back-end server.

| Plugin | README |
| ------ | ------ |
| Dompdf | [https://github.com/barryvdh/laravel-dompdf][PlDb] |
| Passport | [https://laravel.com/docs/8.x/passport][PlGh] |
| Spatie/Laravel-permissions | [https://spatie.be/docs/laravel-permission/v5/introduction][PlGd] |


## License

MIT
