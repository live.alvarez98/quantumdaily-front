import Vue from 'vue'

// axios
import axios from 'axios'

const axiosIns = axios.create({

  baseURL: 'http://quantumdaily-back.test/api/',

  headers: { Authorization: `Bearer ${localStorage.getItem('accessToken')}` }
})
axiosIns.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response.status === 401) {
      return error.response
    }

    return Promise.reject(error);
  }
)
Vue.prototype.$http = axiosIns

export default axiosIns
