import Vue from 'vue'

const hasPermission = (permission) =>{
    let userData = JSON.parse(localStorage.userData)
    if(userData.permissions.find( userPermission => userPermission.name === permission )){
        return true
    }else {
        return false
    }
    
  }
  
export default {
    install (Vue) {
        Vue.prototype.hasPermission = hasPermission
        Vue.hasPermission = hasPermission
    }
}