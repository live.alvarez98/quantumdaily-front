<template>
  <b-card
    title="Registro de entradas y salidas"
  >
      <div class="m-2">
        <b-row>

           <b-row>
            <b-col
              cols="12"
              md="6" 
              class="d-flex align-items-center justify-content-start mb-1 mr-2 mb-md-0"
            >
              <b-button v-if="checkType==='check-in'" variant="success" @click="checkInRegister">
                <feather-icon
                  icon="LogInIcon"
                  size="22"

                />
              </b-button>
              <b-button v-else variant="primary" @click="checkInRegister">
                <feather-icon
                  icon="LogOutIcon"
                  size="22"

                />

              </b-button>
            </b-col>
          </b-row>
          <b-row>

            <b-col
              cols="12"
              md="12"
            >
             <label for="datepicker-full-width">Escoge una fecha para ver el registro de entradas y salidas</label>
              <div class="d-flex align-items-center justify-content-end">
                
                  <b-form-datepicker
                    id="start_date"
                    v-model="date"
                    menu-class="w-100"
                    :state="checkDateRange"
                    calendar-width="100%"
                  />

              </div>
            </b-col>
            </b-row>
        </b-row>
      </div>
    <b-table
      :fields="fields"
      :items="attendance"
      responsive
      :filter="searchQuery"
      class="mb-0"
      show-empty
      empty-text="No se encontraron registros"
    >

      <template #cell(index)="data">
        {{ data.item.id  }}
      </template>

      <template #cell(type)="data">
        <span v-if="data.item.type === 'check-in'">Entrada</span>  
        <span v-else>Salida</span> 
      </template>




    </b-table>
  </b-card>
</template>

<script>
import BCardCode from '@core/components/b-card-code/BCardCode.vue'
import {
  BRow, BCol, BCard,BFormInput, BButton, BTable, BMedia, BAvatar, BLink,
  BBadge, BDropdown, BDropdownItem, BProgress,  BFormDatepicker
} from 'bootstrap-vue'


export default {
  name:'attendance-list',
  components: {
    BCardCode,
    BRow,
    BCol,
    BFormInput,
    BButton,
    BTable,
    BMedia,
    BAvatar,
    BLink,
    BBadge,
    BDropdown,
    BDropdownItem,
    BProgress,
    BFormDatepicker,
    BCard
   


  },
  data() {
    return {
      fields: [
        'index',
        { key: 'date', label: 'Fecha' },
        { key: 'type', label: 'Tipo' },
        { key: 'time', label: 'Hora' },
      ],
      formData:{
        user:'',
        type:'',
        time:'',

      },
      date:'',
      attendance: [],
      today_attendance:[],
      allAttendance: [],
      buttonColor:'success',
      checkType:'check-in',
      searchQuery:'',
      baseURL:'http://quantumdaily-back.test/storage/',

    }
  },
  methods:{
    getAttendance(){
      let userData = JSON.parse(localStorage.userData)
      this.$http.get('user/attendance',{params:{user:userData.id}}).then(res=>{
        this.allAttendance = res.data.attendance
        this.getDates()
        this.attendance = this.allAttendance.filter(attendance => attendance.date == this.date );
      }).catch(err=>{
        console.log(err)
      })
    },
    reload(e){
      this.getAttendance()
    },
    getTodayAttendance(){
        let userData = JSON.parse(localStorage.userData)
        this.$http.get('user/attendance/today',{params:{user:userData.id}}).then(res=>{
          this.today_attendance = res.data.attendance
          
          if(!this.today_attendance){
            this.buttonColor='success'
            this.checkType = 'check-in'
          }else if(this.today_attendance[this.today_attendance.length-1].type==='check-in'){
            this.buttonColor='primary'
            this.checkType = 'check-out'
          }else if(this.today_attendance[this.today_attendance.length-1].type==='check-out'){
            this.buttonColor='success'
            this.checkType = 'check-in'
          }
        }).catch(err=>{
          console.log(err)
      })
      },
    getDates(){
      let todayDate = new Date();
      this.date = todayDate.toISOString().split('T')[0];
    
    },
    checkInRegister(){
      let userData = JSON.parse(localStorage.userData)
      let time =  new Date();
      this.formData.user = userData.id
      this.formData.type=this.checkType
      this.formData.time  = time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
      let alert
     
      if(this.formData.type === 'check-in'){
            alert = this.$swal({
            
              title: '¿Está seguro que desea registrar una entrada?',
              text: "Se registrará la hora actual como hora de entrada",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Si, guardar!',
              customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-outline-danger ml-1',
              },
              buttonsStyling: false,

            })
          }else{
            alert = this.$swal({
              
              title: '¿Está seguro que desea registrar una salida?',
              text: "Se registrará la hora actual como hora de salida",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Si, guardar!',
              customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-outline-danger ml-1',
              },
              buttonsStyling: false,

            })
          }    
          alert.then(result => {
            if (result.value) {
              this.$http.post("user/attendance/checkin", this.formData).then((res) => {
          
                  if (res.data.message === "success") {
                    this.getAttendance()
                    if(this.checkType==='check-in'){
                      this.checkType='check-out'
                      this.$swal({
                          icon: 'success',
                          title: 'Listo!',
                          text: 'La entrada ha sido registrada correctamente.',
                          customClass: {
                            confirmButton: 'btn btn-success',
                          },
                      })
                    }else{
                      this.checkType='check-in'
                      this.$swal({
                          icon: 'success',
                          title: 'Listo!',
                          text: 'La salida ha sido registrada correctamente.',
                          customClass: {
                            confirmButton: 'btn btn-success',
                          },
                      })
                    }
                    
                  }
              }).catch((err) => {
                  console.log(err);
              })
              

              } else {
              this.$swal({
                title: 'Cancelado',
                text: 'El registro no ha sido guardado',
                icon: 'error',
                customClass: {
                  confirmButton: 'btn btn-success',
                },
              })
            }
          })
        



    }
 
  },
  computed:{
    checkDateRange(){
      if(this.date){
        this.attendance = this.allAttendance.filter(attendance => attendance.date == this.date);
      }
    }
  },
  mounted(){
    this.getAttendance()
    this.getTodayAttendance()
    this.getDates()
  }
  


}
</script>

<style lang="scss" scoped>
.per-page-selector {
  width: 90px;
}
</style>

<style lang="scss">
@import '@core/scss/vue/libs/vue-select.scss';
</style>
