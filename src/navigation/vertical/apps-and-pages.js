export default [
  {
    header: 'Admin',
    permissions:['show_roles','create_roles','show_clients','create_clients','show_users'],
  },
  {
    title: 'Usuarios',
    icon: 'UserIcon',
    permissions:['show_users'],
    children: [
      {
        title: 'Listado',
        permissions:['show_users'],
        route: 'apps-users-list',
      },
    
    ],
  },
  
  {
    title: 'Clientes',
    icon: 'UserIcon',
    permissions:['show_clients','create_clients'],
    children: [
      {
        title: 'Listado',
        permissions:['show_clients'],
        route: 'apps-clients-list',
      },
      {
        title: 'Crear',
        permissions:['create_clients'],
        route: 'apps-clients-create',
      },
      
    
    ],
  
  }

,
  {
    title: 'Roles',
    icon: 'FileIcon',
    permissions:['show_roles','create_roles'],
    children: [
       {
        title: 'Listado',
        permissions:['show_roles'],
        route: 'apps-roles-list',
      }, 
      {
        title: 'Crear',
        permissions:['create_roles'],
        route: 'apps-roles-create',
      },
      
    
    ],
  },
  {
    header: 'Proyectos',
  },
  {
    title: 'Proyecto',
    icon: 'ClipboardIcon',
    children: [
      {
        title: 'Listado',
        permissions:['show_projects'],
        route: 'apps-projects-list',
      },
      {
        title: 'Mis proyectos',
        route: 'apps-projects-view-user',
      },
    ],
  },
  {
    title: 'Actividades',
    icon: 'ClipboardIcon',
    children: [
      {
        title: 'Listado',
        route: 'apps-activities-list',
      },
    ],
  },
  {
    title: 'Factura',
    permissions:['show_invoices'],
    icon: 'FileTextIcon',
    children: [
      {
        title: 'Lista',
        permissions:['show_invoices'],
        route: 'apps-invoice-list',
      },
      {
        title: 'Crear',
        permissions:['create_invoices'],
        route: { name: 'apps-invoice-add' },
      },
      {
        title: 'Configuración',
        route: { name: 'apps-taxe-list' },
      }
    ],
  },
  {
    header: 'Asistencias',
  },
  {
    title: 'Asistencia',
    icon: 'FileTextIcon',
    children: [
      {
        title: 'Registro',
        route: 'apps-users-attendance-checkin',
      },
      {
        title: 'Control',
        permissions:['view_users_attendance'],
        route: 'apps-users-attendance-list',
      },
      {
        title: 'Configuración',
        permissions:['config_working_hours'],
        route: 'apps-users-attendance-config',
      },
  
   
    ],
    
    
  },

]
