export default [
  {
    title: 'Dashboard',
    icon: 'HomeIcon',
    route: 'dashboard-user',
    tagVariant: 'light-warning',
  },
]
