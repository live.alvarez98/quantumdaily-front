export default [
  {
    path: '/dashboard/user',
    name: 'dashboard-user',
    component: () => import('@/views/dashboard/user/userDashboard.vue'),
   
  },
  {
    path: '/dashboard/admin',
    name: 'dashboard-admin',
    component: () => import('@/views/dashboard/admin/adminDashboard.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'view_admin_dashboard' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
 
]
