export default [
  // *===============================================---*
  // *--------- USER ---- ---------------------------------------*
  // *===============================================---*
  {
    path: '/apps/users/list',
    name: 'apps-users-list',
    component: () => import('@/views/apps/user/users-list/UsersList.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'show_users' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/users/view/:id',
    name: 'apps-users-view',
    component: () => import('@/views/apps/user/users-view/UsersView.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'show_users' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/users/add',
    name: 'apps-users-add',
    component: () => import('@/views/apps/user/users-add/UsersAdd.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'create_users' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/users/edit/:id',
    name: 'apps-users-edit',
    component: () => import('@/views/apps/user/users-edit/UsersEdit.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'edit_users' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
// *===============================================---*
  // *---------Client ---- ---------------------------------------*
  // *===============================================---*
  {
    path: '/apps/clients/list',
    name: 'apps-clients-list',
    component: () => import('@/views/apps/clients/clients-list/ClientsList.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'show_clients' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/clients/create',
    name: 'apps-clients-create',
    component: () => import('@/views/apps/clients/clients-create/ClientsCreate.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'create_clients' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/clients/edit:id',
    name: 'apps-clients-edit',
    component: () => import('@/views/apps/clients/clients-edit/ClientsEdit.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'edit_clients' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/clients/view:id',
    name: 'apps-clients-view',
    component: () => import('@/views/apps/clients/clients-view/ClientsView.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'show_clients' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },

  // *===============================================---*
  // *---------Role ---- ---------------------------------------*
  // *===============================================---*
  {
    path: '/apps/roles/list',
    name: 'apps-roles-list',
    component: () => import('@/views/apps/roles/roles-list/RolesList.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'show_roles' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/roles/create',
    name: 'apps-roles-create',
    component: () => import('@/views/apps/roles/roles-create/RolesCreate.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'create_roles' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/roles/edit:id',
    name: 'apps-roles-edit',
    component: () => import('@/views/apps/roles/roles-edit/RolesEdit.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'edit_roles' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/roles/view:id',
    name: 'apps-roles-view',
    component: () => import('@/views/apps/roles/roles-view/RolesView.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'show_roles' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
   // *===============================================---*
  // *---------Project activities ---- ---------------------------------------*
  // *===============================================---*
  {
    path: '/apps/activities',
    name: 'apps-activities-list',
    component: () => import('@/views/apps/project_activities/activities-list/ActivitiesList.vue'),
  },
  {
    path: '/apps/project:id/activities/create',
    name: 'apps-project-activities-create',
    component: () => import('@/views/apps/project_activities/activities-create/ActivitiesCreate.vue'),
  },
  {
    path: '/apps/project:id/activities/edit:activitie',
    name: 'apps-project-activities-edit',
    component: () => import('@/views/apps/project_activities/activities-edit/ActivitiesEdit.vue'),
  },
  {
    path: '/apps/project:id/activities/view:activitie',
    name: 'apps-project-activities-view',
    component: () => import('@/views/apps/project_activities/activities-view/ActivitiesView.vue'),
  },
  {
    path: '/apps/activitie/advances:advance',
    name: 'apps-activitie-advances-view',
    component: () => import('@/views/apps/project_activities/advance_activitie/AdvancesView.vue'),
  },
  {
    path: '/apps/activitie/advances/edit:advance',
    name: 'apps-activitie-advances-edit',
    component: () => import('@/views/apps/project_activities/advance_activitie/advance-edit/AdvancesEdit.vue'),
    
  },

  // *===============================================---*
  // *---------  Projects ---------------------------------------*
  // *===============================================---*
  {
    path: '/apps/projects/list',
    name: 'apps-projects-list',
    component: () => import('@/views/apps/projects/project-list/ProjectList.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'show_projects' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/projects/add',
    name: 'apps-projects-add',
    component: () => import('@/views/apps/projects/projects-add/ProjectAdd.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'create_projects' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/projects/edit/:id',
    name: 'apps-projects-edit',
    component: () => import('@/views/apps/projects/projects-edit/ProjectEdit.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'edit_projects' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/projects/view/:id',
    name: 'apps-projects-view',
    component: () => import('@/views/apps/projects/projects-view/ProjectView.vue'),
  },
  {
    path: '/apps/projects/view/user',
    name: 'apps-projects-view-user',
    component: () => import('@/views/apps/projects/projects-user/ProjectsUser.vue'),
  },

  // Invoice
  {
    path: '/apps/invoice/list',
    name: 'apps-invoice-list',
    component: () => import('@/views/apps/invoice/invoice-list/InvoiceList.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'show_invoices' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/invoice/preview/:id',
    name: 'apps-invoice-preview',
    component: () => import('@/views/apps/invoice/invoice-preview/InvoicePreview.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'show_invoices' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/invoice/add/',
    name: 'apps-invoice-add',
    component: () => import('@/views/apps/invoice/invoice-add/InvoiceAdd.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'create_invoices' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/invoice/edit/:id',
    name: 'apps-invoice-edit',
    component: () => import('@/views/apps/invoice/invoice-edit/InvoiceEdit.vue'),
  },
 // *===============================================---*
  // *---------  Taxe ---------------------------------------*
  // *===============================================---*
  {
    path: '/apps/taxe/list',
    name: 'apps-taxe-list',
    component: () => import('@/views/apps/taxe/taxe-list/TaxeList.vue')
  },

  // *===============================================---*
  // *---------  Attendance ---------------------------------------*
  // *===============================================---*
  {
    path: '/apps/attendance',
    name: 'apps-users-attendance-checkin',
    component: () => import('@/views/apps/attendance/attendance-list/AttendanceChecks.vue'),
  },
  {
    path: '/apps/attendance/users',
    name: 'apps-users-attendance-list',
    component: () => import('@/views/apps/attendance/attendance-list/UsersList.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'view_users_attendance' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/attendance/user:id/history',
    name: 'apps-users-attendance-history',
    component: () => import('@/views/apps/attendance/attendance-list/AttendanceHistory.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'view_users_attendance' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/attendance/history:attendance/view',
    name: 'apps-users-attendance-history-view',
    component: () => import('@/views/apps/attendance/attendance-view/AttendanceView.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'view_users_attendance' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },
  {
    path: '/apps/attendance/config',
    name: 'apps-users-attendance-config',
    component: () => import('@/views/apps/attendance/attendance-config/AttendanceConfig.vue'),
    beforeEnter(to, from, next) {
      let userData = JSON.parse(localStorage.userData)
      if (userData.permissions.find( permission => permission.name === 'config_working_hours' )) {
        next()
      } else {
        next({
          name: "misc-not-authorized" // back to safety route //
        });
      }
    }
  },

  

]
